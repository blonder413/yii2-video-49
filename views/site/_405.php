<?php
use yii\helpers\Html;
?>

<?= Html::img(
    '@web/img/405.jpg',
    [
        'alt'   => 'Página No Encontrada'
    ]
) ?>

<p><?= $message; ?></p>