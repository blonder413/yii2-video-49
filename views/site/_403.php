<?php
use yii\helpers\Html;
?>

<?= Html::img(
    '@web/img/403.jpg',
    [
        'alt'   => 'No Tiene acceso a esta página'
    ]
) ?>

<p><?= $message; ?></p>