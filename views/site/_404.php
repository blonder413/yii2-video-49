<?php
use yii\helpers\Html;
?>

<?= Html::img(
    '@web/img/404.jpg',
    [
        'alt'   => 'Página No Encontrada'
    ]
) ?>

<p><?= $message; ?></p>